import collections
from re import S

class SequenceBase(object):
    """Una clase base que representa secuencias biológicas.
    Cuidado: esta clase está sin terminar y se incluye solo como modelo.
    :param s: La secuencia.
    :type s: str
    """

    def __init__(self, s: str):
       self.seq = s.upper()
  
    def complement(self):
        """Calcula el complemento de la secuencia."""
        pass

